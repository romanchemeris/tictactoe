﻿var gameState = {}
gameState.waitingForGame = 0;
gameState.inGame = 1;

User = function (username, userId, state) {
    var self = this;
    self.username = username;
    self.id = userId;
    self.state = state;
}

function makeCellX(id) {
    $("#cell" + id).addClass('x').addClass('btn-info').text('X').removeClass('btn-default');
}

function makeCellO(id) {
    $("#cell" + id).addClass('o').addClass('btn-primary').text('O').removeClass('btn-default');
}

function isCellEmpty(id) {
    return $("#cell" + id).hasClass('btn-default');
}

var n = 7;
var m = 8;
var win_num = 6;
function checkWin(cellNum) {
    var item = $('#cell' + cellNum);
    var x = item.data('coordx');
    var y = item.data('coordy');
    var s = item.text();

    var num_to_win = 0;
    //check col
    for(var i = 0; i < n; i++) {
        if ($('#cell' + x.toString() + i.toString()).text() === s)
            num_to_win++;
        if ($('#cell' + x.toString() + i.toString()).text() !== s)
            num_to_win = 0;
        if (num_to_win === win_num) {
            return true;
        }
    }

    num_to_win = 0;
    //check row
    for (var i = 0; i < m; i++) {
        var curItem = $('#cell' + i.toString() + y.toString()).text();
        if (curItem === s)
            num_to_win++;
        if (curItem !== s)
            num_to_win = 0;
        if (num_to_win === win_num) {
            return true;
        }
    }

    //check diag
    for (var i = 0; i < n; ++i) {
        num_to_win = 0;
        for (var j = 0; i + j < m; ++j) {
            var curItem = $('#cell' + (i + j).toString() + j.toString()).text();
            if (curItem === s)
                num_to_win++;
            if (curItem !== s)
                num_to_win = 0;

            if (num_to_win === win_num) {
                return true;
            }
        }

        num_to_win = 0;
        for (var j = 1; i + j < m; ++j) {
            var curItem = $('#cell' + j.toString() + (i + j).toString()).text();
            if (curItem === s)
                num_to_win++;
            if (curItem !== s)
                num_to_win = 0;

            if (num_to_win === win_num) {
                return true;
            }
        }

        //check anti diag
        num_to_win = 0;
        for (var j = 0; j + i < m; ++j) {
            var curItem = $('#cell' + (i + j).toString() + (m - j - 1).toString()).text();
            if (curItem === s)
                num_to_win++;
            if (curItem !== s)
                num_to_win = 0;

            if (num_to_win === win_num) {
                return true;
            }
        }
    }

    //check anti diag
    for (var i = n; i > 1; i--) {
        for (var j = 0; j + i < m + 1; j++) {
            var curItem = $('#cell' + (m - i - j).toString() + (j).toString()).text();
            if (curItem === s)
                num_to_win++;
            if (curItem !== s)
                num_to_win = 0;

            if (num_to_win === win_num) {
                return true;
            }
        }
    }

    return false;
}

function resetBoard() {
    for (var i = 0; i < n; i++) {
        for (var j = 0; j < m; j++) {
            $("#cell" + i.toString() + j.toString()).removeClass('o').removeClass('x')
                .removeClass('btn-primary').removeClass('btn-info')
                .text('+').addClass('btn-default');
        }
    }
}

function isNoCells() {
    for (var i = 0; i < n; i++) {
        for (var j = 0; j < n; j++)
            if ($("#cell" + i.toString() + j.toString()).text() === '+') {
            return false;
        }
    }
    return true;
}

GameViewModel = function () {
    var self = this;
    self.gameHub = {};
    self.isGameStarted = ko.observable(false);
    self.whosTurn = '';
    self.isMyTurn = ko.observable(false);
    self.currentUser = $('#displayname').val();
    self.users = ko.observableArray();
    self.userToPlay = '';
    self.customRemoveUser = function (userToRemove) {
        var userIdToRemove = userToRemove.id;
        self.users.remove(function (item) {
            return item.id === userIdToRemove;
        });
    }
    self.inviteUserToPlay = function (user) {
        resetBoard();
        self.gameHub.server.inviteUserToGame(user.id);
        self.userToPlay = user.id;
    }

    self.makeTurn = function (data, event) {
        if (self.isGameStarted() && isCellEmpty(data) && self.isMyTurn()) {
            if (self.whosTurn === 'x')
                makeCellX(data);
            else
                makeCellO(data);

            self.isMyTurn(!self.isMyTurn());

            self.gameHub.server.sendTurn(self.userToPlay, data).fail(function (e) { console.log(e); });

            if (checkWin(data)) {
                alert(resources.Win);
                self.isGameStarted(false);
                log(resources.GameEnd, resources.Win);
            }
        }

        if (isNoCells() && self.isGameStarted()) {
            alert(resources.NoOne);
            self.isGameStarted(false);
            log(resources.GameEnd, resources.NoOne);
        }
    }

    self.makeAnotherPlayerTurn = function(cellNum) {
        if (self.isGameStarted() && isCellEmpty(cellNum)) {
            if (self.whosTurn === 'x')
                makeCellO(cellNum);
            else
                makeCellX(cellNum);
            self.isMyTurn(!self.isMyTurn());

            if (checkWin(cellNum)) {
                alert(resources.Loose);
                self.isGameStarted(false);
                log(resources.GameEnd, resources.Loose);

                if (confirm(resources.Revansh)) {
                    self.inviteUserToPlay(new User('', self.userToPlay, 0));
                }
            }
        }

        if (isNoCells() && self.isGameStarted()) {
            alert(resources.NoOne);
            self.isGameStarted(false);
            log(resources.GameEnd, resources.NoOne);
        }
    }
}

function log(time, text) {
    $("#log").append('<li><strong>' + time + '</strong>: ' + htmlEncode(text) + '</li>');
    $("#logDiv").scrollTop($("#logDiv").height() + 20);
}


$(function () {
    var gameHub = $.connection.gameHub;
    var game = new GameViewModel();
    game.gameHub = gameHub;

    gameHub.client.reciveInvite = function (inviterUserId, myId, username, timestamp) {
        log(timestamp, resources.User + " " + htmlEncode(username) + resources.Invite);
        if (confirm(resources.User + ' ' + username + resources.Invite)) {
            resetBoard();
            game.isGameStarted(true);
            game.whosTurn = 'x';
            game.isMyTurn(true);
            game.userToPlay = inviterUserId;

            log(timestamp, resources.StartGame);

            gameHub.server.startGame(inviterUserId, myId);
        }
    };

    gameHub.client.startClientGame = function (inviterUserId, myId, username, timestamp) {
        game.isGameStarted(true);
        game.whosTurn = 'o';
        game.isMyTurn(false);
        log(timestamp, resources.StartGame);
    }

    gameHub.client.makeTurn = function (connectId, cellId) {
        game.makeAnotherPlayerTurn(cellId);
    }

    gameHub.state.username = $('#displayname').val();

    gameHub.client.leaves = function (id, username, timestamp) {
        log(timestamp, resources.UserDisconnected + " - " + htmlEncode(username));
        game.customRemoveUser(new User(username, id));

        if (game.userToPlay === id && game.isGameStarted()) {
            game.isGameStarted(false);
            log(timestamp, resources.UserDisconnectedFromGame);
        }
    }

    gameHub.client.joins = function (id, username, timestamp, state) {
        log(timestamp, resources.UserJoin + " - " + htmlEncode(username));
        game.users.push(new User(username, id, state));
    }

    ko.applyBindings(game);

    $.connection.hub.start()
        .done(function () {

            gameHub.server.getConnectedUsers()
                            .done(function (connectedUsers) {
                                console.log(connectedUsers);
                                ko.utils.arrayForEach(connectedUsers, function (item) {
                                    game.users.push(new User(item.Username, item.Id, item.State));
                                });
                            }).done(function () {
                                gameHub.server.joined();
                            });

        });
});

function htmlEncode(value) {
    var encodedValue = $('<div />').text(value).html();
    return encodedValue;
}