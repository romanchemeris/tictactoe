﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using TicTacToe.Models;
using TicTacToe.Repository;

namespace TicTacToe.Hubs
{
    public class GameHub : Hub
    {
        public void Hello()
        {
            Clients.All.hello();
        }

        public void InviteUserToGame(string connectId)
        {
            var user = InMemoryRepository.CUsers.FirstOrDefault(x => x.Id == Context.ConnectionId);
            Clients.Client(connectId).reciveInvite(Context.ConnectionId, connectId, user.Username, DateTime.Now.ToShortTimeString());
        }

        public void StartGame(string connectId, string inviteUserId)
        {
            var user = InMemoryRepository.CUsers.FirstOrDefault(x => x.Id == inviteUserId);
            Clients.Client(connectId).startClientGame(Context.ConnectionId, connectId, user.Username, DateTime.Now.ToShortTimeString());
        }

        public void SendTurn(string connectId, string cellId)
        {
            Clients.Client(connectId).makeTurn(Context.ConnectionId, cellId);
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var user = InMemoryRepository.CUsers.FirstOrDefault(x => x.Id == Context.ConnectionId);
            if (user != null)
            {
                InMemoryRepository.CUsers.Remove(user);
                return Clients.All.leaves(Context.ConnectionId, user.Username, DateTime.Now.ToShortTimeString());
            }
            return base.OnDisconnected(stopCalled);
        }

        public void Joined()
        {
            InMemoryRepository.CUsers.Add(new GameUser(Context.ConnectionId, Clients.Caller.username));
            Clients.All.joins(Context.ConnectionId, Clients.Caller.username, DateTime.Now.ToShortTimeString(), UserState.WaitingForGame);
        }

        public List<GameUser> GetConnectedUsers()
        {
            return InMemoryRepository.CUsers;
        }
    }

    
}