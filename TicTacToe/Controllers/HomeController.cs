﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExtensionMethods;
using TicTacToe.Helpers;
using Resource = Resources.Resources;

namespace TicTacToe.Controllers
{
    public class HomeController : BaseController
    {
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SetCulture(string culture)
        {
            culture = CultureHelper.GetImplementedCulture(culture);
            HttpCookie cookie = Request.Cookies["_culture"];
            if (cookie != null)
            {
                cookie.Value = culture;
            }
            else
            {
                cookie = new HttpCookie("_culture") {Value = culture, Expires = DateTime.Now.AddYears(1)};
            }
            Response.Cookies.Add(cookie);
            return RedirectToAction("Index");
        }

        public JsonResult GetResources()
        {
            return Json(
                 typeof(Resource)
                    .GetProperties()
                    .Where(p => !p.Name.IsLikeAny("ResourceManager", "Culture"))
                    .ToDictionary(p => p.Name, p => p.GetValue(null) as string)
                 , JsonRequestBehavior.AllowGet);
        }
    }
}