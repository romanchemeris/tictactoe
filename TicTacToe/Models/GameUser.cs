﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicTacToe.Models
{
    public class GameUser
    {
        public GameUser(string id, string username)
        {
            this.Id = id;
            this.Username = username;
            this.State = UserState.WaitingForGame;
        }

        public string Id { get; set; }
        
        public string Username { get; set; }

        public UserState State { get; set; } 
    }

    public enum UserState
    {
        WaitingForGame,
        InGame
    }
}